# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Tcategory(models.Model):
    name = models.CharField(max_length=15)
    author = models.ForeignKey('Tuser', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'tCategory'


class Ttransaction(models.Model):
    datetime = models.DateTimeField()
    amount = models.IntegerField()
    category = models.ForeignKey(Tcategory, models.DO_NOTHING)
    type = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'tTransaction'


class Tuser(models.Model):
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=100)
    email = models.CharField(unique=True, max_length=200)
    encrypted_password = models.CharField(max_length=100)
    active_session_token = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tUser'
